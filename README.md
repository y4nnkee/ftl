## Function Translator

### Description

Einen Übersetzer für Programmiersprachen bei dem möglichst ähnliche, einzelne Funktionen einer anderen
Spache gefunden werden. Für den Moment wird nur **Java** zu **Python** und umgekehrt übersetzt.

* Es soll verschiedene User geben die entweder "contributor" oder "admin" sein können um neue Übersetzungen beizutragen. Momentan wird das nur mit einem Username und einem Passwort möglich sein.
* Es soll ein UI geben für registrierte User ungefähr wie im [Wireframe](doc/ftl_wireframe.pdf) dargestellt. Die Funktion das Passwort und den Username zu ändern sollen momentan noch nicht implementiert werden.
* Neue Übersetzungen müssen von einem admin reviewed werden um sie zu veröffentlichen.
* Eine Übersetzung (Translation) besteht aus einem Set von 2 Funktionen (FunctionEntry), einem Kommentar und einen Bezug auf den User welches diese Translation erstellt hat.
* Ich möchte die Datenbank so initialisieren dass später noch mehr Sprachen hinzu kommen können.

## Installation and run instructions

### Run application

```
./gradlew bootRun
```

### Run tests

```
./gradlew test
```

## Eigenschaften
* Java 16
* Spring Boot 2.5.4
* Pebble
* Bootstrap 4.0.0 (not in use @ the moment)
* Gradle Wrapper

## Functions available at the moment
| JAVA      | PYTHON |
| ----------- | ----------- |
| parseInt      | int       | 
| sum   | sum        |
| max   | max        | false
| getClass   | type        |
| length   | len        | false

## Users available to test
| username      | role |
| ----------- | ----------- |
| superHer0      | contributor       |
| userNo2      | contributor       |
| admin      | admin       |
| password everybody| 123|

