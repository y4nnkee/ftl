//Listener for updating the languages selector
const language_in = document.getElementById('language_in');
const language_out = document.getElementById('language_out');
language_in.addEventListener('input', function () {
    updateLanguagesSelector(language_in, language_out);
}, false);
updateLanguagesSelector(language_in, language_out);

function updateLanguagesSelector(language_in, language_out) {
    let i = language_out.options.length;
    for (i; i >= 0; i--) {
        language_out.options.remove(i);
    }
    fetch('/api/translation/languages')
        .then(response => response.json())
        .then(data => {
            let lang_list = Object.keys(data);
            let res = [];
            for(let lang of lang_list) {
                if(lang !== language_in.value) {
                    res.push(lang);
                }
            }
            for(let l of res) {
                const o = document.createElement("option");
                o.text = l;
                o.value =l;
                language_out.options.add(o, o.text);
            }
        });
}