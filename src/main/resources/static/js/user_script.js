function showChangeForm(id) {
    let changetab = document.getElementById(id);
    changetab.style.visibility = "visible";
}

function deleteTranslation(id) {
    if (confirm('Are you sure you want to delete the Translation with id ' + id + '?')) {
        const fieldCount = 6;
        let form = document.forms.namedItem("publish_form_" + id);
        const csrfToken = form.elements[2*fieldCount+1].value;
        fetch('/api/translation', {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': csrfToken
            },
            body: id
        }).then(response => {
            if (!response.ok) {
                throw new Error(response.error().toString());
            } else {
                location.reload();
            }
        })
    }
}

//Listener for updating the libraries
const language_a = document.getElementById('function_language_a');
const library_a = document.getElementById('function_library_a');
language_a.addEventListener('input', function () {
    updateLibraries(language_a, library_a);
}, false);
const language_b = document.getElementById('function_language_b');
const library_b = document.getElementById('function_library_b');
language_b.addEventListener('input', function () {
    updateLibraries(language_b, library_b);
}, false);

function updateLibraries(language, library) {
    let i = library.options.length;
    for (i; i >= 0; i--) {
        library.options.remove(i);
    }
    let lib_list = fetch('/api/translation/languages')
        .then(response => response.json())
        .then(data => {
            for (const [key, value] of Object.entries(data)) {
                if (language.value === key) {
                    for (let i = 0; i < value.length; i++) {
                        const o = document.createElement("option");
                        o.text = value[i];
                        o.value = value[i];
                        library.options.add(o, o.text);
                    }
                }
            }
        });
}

function submitTranslation() {
    const fieldCount = 5;
    let form = document.forms.namedItem("new_translation_form");
    let translation = getTranslationObject(form, fieldCount);
    const csrfToken = form.elements[2*fieldCount+1].value;

    fetch('/api/translation', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': csrfToken
        },
        body: JSON.stringify(Object.fromEntries(translation)),
    }).then(function (response) {
        return response.text();
    }).then(function (data) {
        createHint(data);
    });
}

function publishTranslation(form_id) {
    const fieldCount = 6;
    let form = document.forms.namedItem("publish_form_" + form_id);
    let translation = getTranslationObject(form, fieldCount);
    translation.set("id", form_id);
    const csrfToken = form.elements[2*fieldCount+1].value;

    fetch('/api/translation', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': csrfToken
        },
        body: JSON.stringify(Object.fromEntries(translation)),
    }).then(response => {
        if(response.status === 202) {
            removeTranslation(form_id);
        }
        return response.text();
    }).then(function (data) {
        createHint(data);
    });

}

function getTranslationObject(form, fieldcount) {
    const translation = new Map();
    const function_a = new Map();
    const function_b = new Map();
    for (let i = 0; i < fieldcount; i++) {
        let f_a = form.elements[i];
        let f_b = form.elements[i + fieldcount];
        if (f_a.required === true && (f_a.value === '' || f_a.value === null)) {
            createHint(f_a.getAttribute("name") + ' can\'t be empty');
            throw new Error(f_a.getAttribute("name") + ' can\'t be empty');
        }
        if (f_b.required === true && (f_b.value === '' || f_b.value === null)) {
            createHint(f_b.getAttribute("name") + ' can\'t be empty');
            throw new Error(f_b.getAttribute("name") + ' can\'t be empty');
        }
        function_a.set(form.elements[i].getAttribute("name"), form.elements[i].value);
        function_b.set(form.elements[fieldcount + i].getAttribute("name"), form.elements[fieldcount + i].value);
    }
    translation.set("comment", form.elements[2 * fieldcount].value);
    translation.set("functions", [Object.fromEntries(function_a), Object.fromEntries(function_b)]);
    return translation;
}

function createHint(data) {
    let mainDiv = document.getElementById('main_div');
    let mainCont = document.getElementById('main_container')
    let oldHint = document.getElementById('hint_div');
    if (oldHint !== null) {
        mainCont.removeChild(oldHint);
    }
    let hintDiv = document.createElement('div');
    hintDiv.setAttribute('class', 'hint');
    hintDiv.setAttribute('id', 'hint_div');
    let newContent = document.createTextNode(data);
    hintDiv.appendChild(newContent);
    mainCont.insertBefore(hintDiv, mainDiv);
}

function removeTranslation(id) {
    let updateDiv = document.getElementById('update_id');
    let translationDiv = document.getElementById('translation_div_'+id);
    updateDiv.removeChild(translationDiv);
}