package com.function_translator.yannhoh.repository;

import com.function_translator.yannhoh.model.FunctionEntry;
import com.function_translator.yannhoh.model.Translation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * This interface the defines the access for the Translations stored in the db
 */
@Repository
public interface TranslationRepository extends JpaRepository<Translation, Long> {

    Optional<List<Translation>> findTranslationByFunctionsContainsAndEnabledIsTrue(FunctionEntry functionA);

    Optional<Translation> findTranslationByFunctionsContainsAndFunctionsContains(FunctionEntry funcA, FunctionEntry funcB);

    Optional<List<Translation>> findAllByUsername(String userName);

    Optional<List<Translation>> findAllByEnabledIsFalse();

    Optional<Translation> findById(long id);

    void deleteById(long id);


}
