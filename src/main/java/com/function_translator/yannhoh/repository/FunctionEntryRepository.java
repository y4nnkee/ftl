package com.function_translator.yannhoh.repository;

import com.function_translator.yannhoh.model.FunctionEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * This interface the defines the access for the Functions stored in the db
 *
 */
@Repository
public interface FunctionEntryRepository extends JpaRepository<FunctionEntry, Long> {

    Optional<FunctionEntry> findBySearchNameAndLanguage(String searchName, String language);

    Optional<FunctionEntry> findById(long id);
}
