package com.function_translator.yannhoh.security;

/**
 * This enum class defines the available permissions a user can have
 * Inspired by: https://www.youtube.com/watch?v=her_7pa0vrg&t=8053s
 */
public enum UserPermissions {
    TRANSLATIONS_READ("translations:read"),
    TRANSLATIONS_WRITE("translations:write"),
    TRANSLATIONS_UPDATE("translations:update"),

    CONTRIBUTOR_WRITE("contributor:write"),     //registration
    CONTRIBUTOR_READ("contributor:read"),       //ContributorDetails
    CONTRIBUTOR_UPDATE("contributor:update");   //Not implemented yet

    private final String permission;

    UserPermissions(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }

}
