package com.function_translator.yannhoh.security;

import com.google.common.collect.Sets;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

import static com.function_translator.yannhoh.security.UserPermissions.*;

/**
 * This enum class defines the user roles and their associated permissions
 */
public enum UserRoles {

    PUBLIC(Sets.newHashSet(TRANSLATIONS_READ, CONTRIBUTOR_WRITE)),
    CONTRIBUTOR(Sets.newHashSet(TRANSLATIONS_READ, TRANSLATIONS_WRITE,
            CONTRIBUTOR_READ, CONTRIBUTOR_UPDATE)),
    ADMIN(Sets.newHashSet(TRANSLATIONS_READ, TRANSLATIONS_WRITE, TRANSLATIONS_UPDATE,
            CONTRIBUTOR_READ, CONTRIBUTOR_UPDATE, CONTRIBUTOR_WRITE));

    private final Set<UserPermissions> userPermissionsSet;

    UserRoles(Set<UserPermissions> permissionsSet) {
        this.userPermissionsSet = permissionsSet;
    }

    public Set<UserPermissions> getUserPermissionsSet() {
        return userPermissionsSet;
    }

    public Set<GrantedAuthority> getGrantedAuthorities() {
        Set<GrantedAuthority> collect = getUserPermissionsSet().stream()
                .map(permissions -> new SimpleGrantedAuthority(permissions.getPermission())
                ).collect(Collectors.toSet());
        collect.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return collect;
    }
}
