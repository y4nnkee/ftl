package com.function_translator.yannhoh.security;


import com.function_translator.yannhoh.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * This class configures the security for the application
 * It extends the WebSecurityConfigAdapter
 *
 * Inspired by: https://www.youtube.com/watch?v=her_7pa0vrg&t=8053s
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)  //For the use of annotations in controller for config
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private DaoAuthenticationProvider daoAuthenticationProvider;

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.authenticationProvider(this.daoAuthenticationProvider);
    }

    /**
     * Method to configure an WebSecurityConfigAdapter.
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .loginPage("/login").permitAll()
                .defaultSuccessUrl("/user/profile")
                .and().authorizeRequests()
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
                .mvcMatchers("/h2/**").hasRole(UserRoles.ADMIN.name())
                .mvcMatchers("/", "/translation/**", "/about").permitAll()
                .mvcMatchers("/api/translation").hasAnyRole(UserRoles.ADMIN.name(), UserRoles.CONTRIBUTOR.name())
                .mvcMatchers("/api/translation/languages").permitAll()
                .anyRequest().authenticated();

        http.headers().frameOptions().disable();    //This line is to be able to see H2 console in browser
    }

    /**
     * Method to configure an AuthenticationProvider.
     */
    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        var daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(this.userService);
        daoAuthenticationProvider.setPasswordEncoder(this.passwordEncoder);
        return daoAuthenticationProvider;
    }

    /**
     * Method which creates a password encoder
     *
     * @return ByCryptPasswordEncoder in order to encode and decode the plain text passwords
     */
    @Bean
    public BCryptPasswordEncoder pwEncoder() {
        return new BCryptPasswordEncoder(10);
    }

}
