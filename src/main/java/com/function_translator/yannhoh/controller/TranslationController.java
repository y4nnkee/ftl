package com.function_translator.yannhoh.controller;

import com.function_translator.yannhoh.exception.APINotFoundException;
import com.function_translator.yannhoh.model.FunctionEntry;
import com.function_translator.yannhoh.model.Language;
import com.function_translator.yannhoh.model.Translation;
import com.function_translator.yannhoh.service.TranslationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Set;

/**
 * This class is the controller of the translation domain pages
 */
@Controller
@RequestMapping("/translation")
public class TranslationController {

    private final TranslationService translationService;

    @Autowired
    public TranslationController(TranslationService translationService) {
        this.translationService = translationService;
    }

    /**
     * Get mapping for the search of a translation with path parameters
     * returns a string for the text area
     */
    @GetMapping("/search{language_in}{search_name}{language_out}")
    public String searchTranslation(@RequestParam("search_name") String functionA,
                                    @RequestParam("language_in") String fromLanguage,
                                    @RequestParam("language_out") String toLanguage,
                                    Model model) {
        try {
            FunctionEntry f = translationService.searchFunction(functionA, fromLanguage);
            Translation t = translationService.searchTranslation(f, toLanguage);
            model.addAttribute("result", getResultOutput(t, f));
        } catch (APINotFoundException e) {
            model.addAttribute("result", e.getMessage());
        }
        model.addAttribute("languages", Language.getAllLanguages());
        return "index";
    }

    /**
     * This method creates the output string for the search function
     *
     * @param f Function which has been searched to translate
     * @param t Translation which is the result of the search
     */
    static String getResultOutput(Translation t, FunctionEntry f) {
        Set<FunctionEntry> s = t.getFunctions();
        FunctionEntry resultFunc = null;
        s.remove(f);    //Remove the input function
        for (FunctionEntry ff : s) {
            resultFunc = ff;
        }
        assert resultFunc != null;
        return resultFunc.getLanguage() + "\n" + resultFunc.getFullName() + "\n" + resultFunc.getLibrary() +
                "\n" + resultFunc.getDocUrl();
    }
}
