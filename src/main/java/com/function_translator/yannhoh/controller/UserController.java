package com.function_translator.yannhoh.controller;

import com.function_translator.yannhoh.model.Language;
import com.function_translator.yannhoh.service.TranslationService;
import com.function_translator.yannhoh.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This class is the Controller of the user domain.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final TranslationService translationService;

    @Autowired
    public UserController(UserService userService, TranslationService translationService) {
        this.userService = userService;
        this.translationService = translationService;
    }

    /**
     * Get mapping for the profile page
     */
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/profile")
    public String userHome(Authentication authentication, Model model) {
        model.addAttribute("users_name", authentication.getName());
        model.addAttribute("translation_list", translationService.getAllByUsername(authentication.getName()));
        return "user/user_profile";
    }

    /**
     * Get mapping for the contribute page
     */
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/contribute")
    public String userContribute(Authentication auth, Model model) {
        model.addAttribute("languages", Language.getAllLanguages());
        if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("translations:update"))) {
            model.addAttribute("translation_list_not_public", translationService.getAllUnpublic());
        }
        return "user/user_contribute";
    }
}
