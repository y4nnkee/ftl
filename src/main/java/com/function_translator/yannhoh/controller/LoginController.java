package com.function_translator.yannhoh.controller;

import com.function_translator.yannhoh.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

/**
 * This class is the Controller of the Login process.
 */
@Controller
public class LoginController {

    public LoginController(UserService userService) {
    }

    /**
     * Mapping and redirecting if successful login
     */
    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public String login(Authentication authentication, @RequestParam("error") Optional<String> error, Model model) {
        if (authentication != null && authentication.isAuthenticated()) {
            return "redirect:/user/profile";
        } else {
            model.addAttribute("hasLoginError", error.isPresent());
            return "login/login";
        }
    }
}
