package com.function_translator.yannhoh.controller;

import com.function_translator.yannhoh.exception.APIAlreadyReportedException;
import com.function_translator.yannhoh.exception.APINotFoundException;
import com.function_translator.yannhoh.model.Language;
import com.function_translator.yannhoh.model.Translation;
import com.function_translator.yannhoh.service.TranslationService;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is the REST Controller of the translation domain
 */
@RequestMapping(path = "/api/translation", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@Tag(name = "Translation API", description = "Provides CRUD support for translation entities.")
public class TranslationRestController {

    private final TranslationService translationService;

    @Autowired
    public TranslationRestController(TranslationService translationService) {
        this.translationService = translationService;
    }

    /**
     * PUT mapping to publish (update) a unpublic translation
     * Only reachable as with the user role ADMIN
     */
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponse(responseCode = "400", content = @Content, description = "The translation in the request body contains validation errors.")
    @ApiResponse(responseCode = "404", content = @Content(mediaType = "text/plain"))
    @ApiResponse(responseCode = "202", content = @Content(mediaType = "text/plain", schema = @Schema(implementation = Translation.class)))
    public ResponseEntity<Object> publishTranslation(@Valid @RequestBody Translation translation, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        translation.setCreatedOn(Date.valueOf(LocalDate.now()).toString());
        try {
            translationService.publishTranslation(translation);
            String body = "Thank you for the review of the translation. It is now public and available with the public search function.";
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(body);
        } catch (APINotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e.getCause());
        }
    }

    /**
     * POST mapping to add (submit) a new translation
     * Only reachable as with the user role CONTRIBUTOR and ADMIN
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponse(responseCode = "400", content = @Content, description = "The translation in the request body contains validation errors.")
    @ApiResponse(responseCode = "409", content = @Content(mediaType = "text/plain"))
    @ApiResponse(responseCode = "201", content = @Content(mediaType = "text/plain",
            schema = @Schema(implementation = Translation.class)))
    public ResponseEntity<Object> addNewTranslation(@Valid @RequestBody Translation translation,
                                                    Authentication authentication, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        try {
            translationService.addNewTranslation(translation, authentication.getName());
            String body = "Thank you for contributing a new translation. You can now see it under \"Profile\". It has to be checked by a admin to get public.";
            return ResponseEntity.status(HttpStatus.CREATED).body(body);
        } catch (APIAlreadyReportedException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }

    /**
     * DELETE mapping to delete a new translation
     * Only reachable as with the user role ADMIN
     */
    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponse(responseCode = "404", content = @Content(mediaType = "text/plain"))
    @ApiResponse(responseCode = "202", content = @Content(mediaType = "text/plain"))
    public ResponseEntity<Object> deleteTranslation(@RequestBody String id) {
        long idLong = Long.parseLong(id);
        try {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(translationService.deleteTranslationId(idLong));
        } catch (APINotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(id);
        }
    }

    /**
     * GET mapping for languages
     * Reachable for everyone
     * returns the actual available languages and libraries
     */
    @GetMapping(value = "/languages")
    @ApiResponse(responseCode = "404", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
    @ApiResponse(responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
    public ResponseEntity<Map<String, List<String>>> getAllLanguages() {
        Map<String, List<String>> response = new HashMap<>();
        String[] languages = Language.getAllLanguages();
        for (String lang : languages) {
            var liblist = Language.getLanguage(lang).getLibrariesList();
            if (liblist.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            response.put(lang, liblist.get());
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

}
