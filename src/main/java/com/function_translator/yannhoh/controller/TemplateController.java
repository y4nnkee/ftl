package com.function_translator.yannhoh.controller;

import com.function_translator.yannhoh.model.Language;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This class is the Controller for the public mapping
 *
 */
@Controller
@RequestMapping("/")
public class TemplateController {

    /**
     * Get mapping for the index/home page
     */
    @GetMapping
    public String index(Model model) {
        model.addAttribute("languages", Language.getAllLanguages());
        return "index";
    }

    /**
     * Get mapping for the about page
     */
    @GetMapping("about")
    public String about(Model model) {
        return "about";
    }

}
