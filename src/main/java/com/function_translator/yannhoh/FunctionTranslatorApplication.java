package com.function_translator.yannhoh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This class runs the application
 */
@SpringBootApplication
public class FunctionTranslatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(FunctionTranslatorApplication.class, args);
    }

}
