package com.function_translator.yannhoh.exception;


public class APIAlreadyReportedException extends RuntimeException {
    public APIAlreadyReportedException(String message) {
        super(message);
    }
}