package com.function_translator.yannhoh.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;

/**
 * This class defines own exceptions which return ResponseEntities
 * with the responsible http status code and message
 */
@ControllerAdvice
public class APIExceptionHandler {

    @ExceptionHandler(value = {APINotAcceptableException.class})
    public ResponseEntity<Object> handleExceptionNotAcceptable(APINotAcceptableException e) {
        HttpStatus notAcceptable = HttpStatus.NOT_ACCEPTABLE;
        APIException apiException = new APIException(e.getMessage(), notAcceptable, ZonedDateTime.now());
        return new ResponseEntity<>(apiException, notAcceptable);
    }

    @ExceptionHandler(value = {APINotFoundException.class})
    public ResponseEntity<Object> handleExceptionNotFound(APINotFoundException e) {
        HttpStatus notFound = HttpStatus.NOT_FOUND;
        APIException apiException = new APIException(e.getMessage(), notFound, ZonedDateTime.now());
        return new ResponseEntity<>(apiException, notFound);
    }

    @ExceptionHandler(value = {APIAlreadyReportedException.class})
    public ResponseEntity<Object> handleExceptionNotFound(APIAlreadyReportedException e) {
        HttpStatus alreadyReported = HttpStatus.ALREADY_REPORTED;
        APIException apiException = new APIException(e.getMessage(), alreadyReported, ZonedDateTime.now());
        return new ResponseEntity<>(apiException, alreadyReported);
    }

}
