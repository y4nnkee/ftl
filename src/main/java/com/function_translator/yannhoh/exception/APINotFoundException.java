package com.function_translator.yannhoh.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class APINotFoundException extends RuntimeException {
    public APINotFoundException(String message) {
        super(message);
    }
}