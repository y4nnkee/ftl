package com.function_translator.yannhoh.exception;


public class APINotAcceptableException extends RuntimeException {
    public APINotAcceptableException(String message) {
        super(message);
    }
}