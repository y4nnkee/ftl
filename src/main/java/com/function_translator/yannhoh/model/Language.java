package com.function_translator.yannhoh.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * This enum class defines the available languages and their libraries
 * It could be saved on the db as well
 */
public enum Language {
    JAVA(Map.of("UTIL", "util", "LANG", "lang")),
    PYTHON(Map.of("STANDARD", "Standard_Library", "NUMPY", "NumPy"));

    private final Map<String, String> libraries;

    Language(Map<String, String> libraries) {
        this.libraries = libraries;
    }

    public static Language getLanguage(String language) {
        Language result = null;
        if (language.equals("JAVA")) result = JAVA;
        if (language.equals("PYTHON")) result = PYTHON;
        return result;
    }

    public Map<String, String> getLibraries() {
        return libraries;
    }

    public Optional<List<String>> getLibrariesList() {
        return Optional.of(new ArrayList<>(libraries.keySet()));
    }

    public static String[] getAllLanguages() {
        return Stream.of(Language.values()).map(Language::name).toArray(String[]::new);
    }

}
