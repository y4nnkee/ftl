package com.function_translator.yannhoh.model;

import com.function_translator.yannhoh.security.UserRoles;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Set;
/**
 * This class is a model class for a user.
 * It defines the db entry
 * It implements the UserDetails interface in order to work with the user service
 *
 * inspired by: https://www.youtube.com/watch?v=her_7pa0vrg&t=8053s
 *
 */
@Entity
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 15, nullable = false)
    private String username;
    @Column(nullable = false)
    private String password;
    private Date createdOn;
    private boolean isAccountNonExpired, isAccountNonLocked, isCredentialsNonExpired, isEnabled;
    @ElementCollection
    private Set<GrantedAuthority> grantedAuthorities;


    public User(String username, String password) {
        this.grantedAuthorities = UserRoles.CONTRIBUTOR.getGrantedAuthorities();
        this.username = username;
        this.password = password;
        this.createdOn = Date.valueOf(LocalDate.now());
        this.isAccountNonExpired = true;
        this.isAccountNonLocked = true;
        this.isCredentialsNonExpired = true;
        this.isEnabled = true;
    }

    public User(Set<GrantedAuthority> grantedAuthorities, String username, String password,
                boolean isAccountNonExpired, boolean isAccountNonLocked, boolean isCredentialsNonExpired,
                boolean isEnabled) {
        this.grantedAuthorities = grantedAuthorities;
        this.username = username;
        this.password = password;
        this.createdOn = Date.valueOf(LocalDate.now());
        this.isAccountNonExpired = isAccountNonExpired;
        this.isAccountNonLocked = isAccountNonLocked;
        this.isCredentialsNonExpired = isCredentialsNonExpired;
        this.isEnabled = isEnabled;

    }

    public User() {
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        isAccountNonExpired = accountNonExpired;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        isAccountNonLocked = accountNonLocked;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        isCredentialsNonExpired = credentialsNonExpired;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isAccountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public Set<GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public void setId(Long id) {
        this.id = id;
    } //Only for testing purposes

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", createdOn=" + createdOn +
                ", isAccountNonExpired=" + isAccountNonExpired +
                ", isAccountNonLocked=" + isAccountNonLocked +
                ", isCredentialsNonExpired=" + isCredentialsNonExpired +
                ", isEnabled=" + isEnabled +
                ", grantedAuthorities=" + grantedAuthorities +
                '}';
    }
}
