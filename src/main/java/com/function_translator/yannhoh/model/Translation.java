package com.function_translator.yannhoh.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * This class is a model class for a translation which includes 2 functions
 * It defines the db entry
 * It contains only getter and setter
 */
@Entity
public class Translation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(nullable = false)
    @ManyToMany
    @NotEmpty
    private Set<FunctionEntry> functions = new HashSet<>();
    @Column(columnDefinition = "TEXT")
    private String comment;
    private String username;
    private String createdOn;
    @Column(nullable = false)
    private boolean enabled;

    public Translation(String userName, Set<FunctionEntry> functionsSet, String comment) {
        this.username = userName;
        this.functions = functionsSet;
        this.comment = comment;
        this.enabled = false;
        this.createdOn = Date.valueOf(LocalDate.now()).toString();
    }

    public Translation() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<FunctionEntry> getFunctions() {
        return functions;
    }

    public void setFunctions(Set<FunctionEntry> functions) {
        this.functions = functions;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "Translation{" +
                "id=" + id +
                ", functions=" + functions +
                ", comment='" + comment + '\'' +
                ", username='" + username + '\'' +
                ", createdOn=" + createdOn +
                ", enabled=" + enabled +
                '}';
    }

}
