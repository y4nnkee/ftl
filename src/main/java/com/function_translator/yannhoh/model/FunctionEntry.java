package com.function_translator.yannhoh.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * This class is a model class for a function.
 * It defines the db entry
 * It contains only getter and setter
 *
 */
@Entity
public class FunctionEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(nullable = false)
    @NotEmpty
    private String searchName;
    @Column(nullable = false)
    @NotEmpty
    private String fullName;
    @Column(nullable = false)
    @NotEmpty
    private String docUrl;
    @Column(nullable = false)
    @NotEmpty
    private String language;
    @Column(nullable = false)
    @NotEmpty
    private String library;
    @Column(nullable = false)
    private boolean enabled;

    public FunctionEntry(String searchName, String fullName, String docUrl, Language language, String library) {
        this.searchName = searchName;
        this.fullName = fullName;
        this.docUrl = docUrl;
        this.language = language.name();
        this.library = library;
        this.enabled = false;
    }

    public FunctionEntry() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String function_name) {
        this.searchName = function_name;
    }

    public String getDocUrl() {
        return docUrl;
    }

    public void setDocUrl(String docUrl) {
        this.docUrl = docUrl;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLibrary() {
        return library;
    }

    public void setLibrary(String library) {
        this.library = library;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getFullName() { return fullName; }

    public void setFullName(String fullName) { this.fullName = fullName; }

    @Override
    public String toString() {
        return "FunctionEntry{" +
                "id=" + id +
                ", searchName='" + searchName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", docUrl='" + docUrl + '\'' +
                ", language='" + language + '\'' +
                ", library='" + library + '\'' +
                ", enabled=" + enabled +
                '}';
    }

}
