package com.function_translator.yannhoh.service;

import com.function_translator.yannhoh.exception.APIAlreadyReportedException;
import com.function_translator.yannhoh.exception.APIException;
import com.function_translator.yannhoh.exception.APINotFoundException;
import com.function_translator.yannhoh.model.FunctionEntry;
import com.function_translator.yannhoh.model.Language;
import com.function_translator.yannhoh.model.Translation;
import com.function_translator.yannhoh.repository.FunctionEntryRepository;
import com.function_translator.yannhoh.repository.TranslationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.*;

/**
 * This class is the logical layer between the Translation controller and the repository
 * It also uses the function repository
 */
@Service
public class TranslationService {

    private final TranslationRepository translationRepository;
    private final FunctionEntryRepository functionEntryRepository;

    @Autowired
    public TranslationService(TranslationRepository translationRepository, FunctionEntryRepository functionEntryRepository) {
        this.translationRepository = translationRepository;
        this.functionEntryRepository = functionEntryRepository;
    }

    /**
     * Method to search a function based on the input of the user
     * It ignores special characters and lower/uppercase in the searchname
     *
     * @param searchName   = input string for the function to translate
     * @param fromLanguage = input string for the language of the function in the
     * @return FunctionEntry
     */
    public FunctionEntry searchFunction(String searchName, String fromLanguage) {
        String searchString = searchName.toLowerCase().replaceAll("[\\[\\]().,]", "");
        Optional<FunctionEntry> function = functionEntryRepository.findBySearchNameAndLanguage(searchString, fromLanguage);
        if (function.isEmpty()) {
            throw new APINotFoundException("Function " + searchName + " not found");
        }
        return function.get();
    }

    /**
     * Method to search a translation based on the input function
     *
     * @param function    = function which has to be found beforehand can not be null
     * @param languageOut = input string for the desired output language
     * @return Translation
     */
    public Translation searchTranslation(FunctionEntry function, String languageOut) {
        assert function != null;
        Optional<List<Translation>> translations = translationRepository.findTranslationByFunctionsContainsAndEnabledIsTrue(function);
        Set<FunctionEntry> funcSet = new HashSet<>(2);
        Translation result = null;
        if (translations.isEmpty()) {
            throw new APINotFoundException("Translation not found");
        }
        Language language = Language.getLanguage(languageOut);
        //Loop through translations to find the one with the correct language on the other function
        for (Translation t : translations.get()) {
            funcSet.addAll(t.getFunctions());
            funcSet.remove(function);
            for (FunctionEntry func : funcSet) {
                if (func.getLanguage().equals(language.name())) {
                    result = t;
                    break;
                }
            }
        }
        if (result == null) throw new APINotFoundException("No translation found");
        return result;
    }

    /**
     * Method to get all contributed translations of a user
     *
     * @param username = String
     * @return List of translations
     */
    public List<Translation> getAllByUsername(String username) {
        Optional<List<Translation>> optionalList = translationRepository.findAllByUsername(username);
        if (optionalList.isEmpty()) {
            throw new APINotFoundException("No translations with this user id found"); //Empty list is returned
        }
        return optionalList.get();
    }

    /**
     * Method to get all unpublished translations.
     *
     * @return List of translations
     */
    public List<Translation> getAllUnpublic() {
        Optional<List<Translation>> optionalList = translationRepository.findAllByEnabledIsFalse();
        if (optionalList.isEmpty()) {
            throw new APINotFoundException("No unpublic translations found");
        }
        return optionalList.get();
    }

    /**
     * Method to delete a translation based on id
     *
     * @param id
     * @return id of the deleted translation
     */
    @Modifying
    public long deleteTranslationId(long id) throws APINotFoundException {
        Optional<Translation> transl = translationRepository.findById(id);
        if (transl.isPresent()) {
            translationRepository.deleteById(id);
            return id;
        } else {
            throw new APINotFoundException("No translations with this id found");
        }
    }

    /**
     * Method to find out if a translation exists
     *
     * @param id
     * @return boolean
     */
    public Boolean exists(long id) {
        return translationRepository.findById(id).isPresent();
    }

    /**
     * Method to publish/update a translation
     *
     * @param translation = Translation which has already an id
     * @return updated translation
     */
    @Modifying
    @Transactional
    public Translation publishTranslation(Translation translation) throws APINotFoundException, APIException {
        Optional<Translation> translOpt = translationRepository.findById(translation.getId());
        if (translOpt.isEmpty()) {
            throw new APINotFoundException("No translation with this id found");
        }
        List<FunctionEntry> functions = translation.getFunctions().stream().toList();
        if (functions.size() != 2) {
            throw new APIException("function set not complete", HttpStatus.BAD_REQUEST, ZonedDateTime.now());
        }
        FunctionEntry fARequest = functions.get(0);
        FunctionEntry fBRequest = functions.get(1);
        Optional<FunctionEntry> funcAOpt = functionEntryRepository.findById(fARequest.getId());
        Optional<FunctionEntry> funcBOpt = functionEntryRepository.findById(fBRequest.getId());
        if (funcAOpt.isEmpty() || funcBOpt.isEmpty()) {
            throw new APINotFoundException("No function with this id found");
        }

        Set<FunctionEntry> set = new HashSet<>(2);
        FunctionEntry funcA = setFunction(fARequest, funcAOpt);
        FunctionEntry funcB = setFunction(fBRequest, funcBOpt);

        assert funcA != null;
        functionEntryRepository.save(funcA);
        assert funcB != null;
        functionEntryRepository.save(funcB);
        set.add(funcA);
        set.add(funcB);

        Translation newTranslation = translOpt.get();
        newTranslation.setFunctions(set);
        newTranslation.setEnabled(true);
        newTranslation.setComment(translation.getComment());
        return translationRepository.save(newTranslation);
    }

    static FunctionEntry setFunction(FunctionEntry fARequest, Optional<FunctionEntry> funcAOpt) {
        if (funcAOpt.isPresent()) {
            FunctionEntry funcA = funcAOpt.get();
            funcA.setSearchName(fARequest.getSearchName());
            funcA.setFullName(fARequest.getFullName());
            funcA.setDocUrl(fARequest.getDocUrl());
            funcA.setLanguage(fARequest.getLanguage());
            funcA.setLibrary(fARequest.getLibrary());
            funcA.setEnabled(true);
            return funcA;
        } else {
            return null;
        }
    }

    /**
     * Method to add a new translation
     *
     * @param translation = Translation without existing id
     * @param username    = username of the contributor
     * @return created translation
     */
    @Modifying
    public Translation addNewTranslation(Translation translation, String username) throws APIAlreadyReportedException {
        Set<FunctionEntry> functionsNew = translation.getFunctions();
        //Creating a new functionEntry if it does not exist
        Stack<FunctionEntry> functionFromDbStack = new Stack<>();
        var newCreatedFunctions = false;
        Set<FunctionEntry> setForNewTransl = new HashSet<>(2);
        List<FunctionEntry> funcList = new ArrayList<>(2);
        for (FunctionEntry func : functionsNew) {
            if (functionEntryRepository.findBySearchNameAndLanguage(func.getSearchName(), func.getLanguage()).isEmpty()) {
                functionEntryRepository.save(func);
                setForNewTransl.add(func);
                newCreatedFunctions = true;
            } else {
                func = functionEntryRepository.findBySearchNameAndLanguage(func.getSearchName(), func.getLanguage()).get();
                setForNewTransl.add(func);
            }
            functionFromDbStack.push(func);
            funcList.add(func);
        }
        if (!newCreatedFunctions) {
            Optional<Translation> optionalTranslation = translationRepository
                    .findTranslationByFunctionsContainsAndFunctionsContains(functionFromDbStack.pop(), functionFromDbStack.pop());
            if (optionalTranslation.isPresent()) {
                throw new APIAlreadyReportedException("Translation with the functions " + funcList.get(0).getFullName() + " and " + funcList.get(1).getFullName() + " is already reported");
            }
        }
        //Create new translation and save it
        Translation newTranslationForDb = new Translation(username, setForNewTransl, translation.getComment());
        return translationRepository.save(newTranslationForDb);
    }

}
