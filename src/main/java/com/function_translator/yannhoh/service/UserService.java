package com.function_translator.yannhoh.service;

import com.function_translator.yannhoh.model.User;
import com.function_translator.yannhoh.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class is the logical layer between the User controller and the repository
 * <p>
 * inspired by: https://www.youtube.com/watch?v=her_7pa0vrg&t=8053s
 */
@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
        BCryptPasswordEncoder pwEncoder = new BCryptPasswordEncoder(10);
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findUserByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username " + username + " not found."));
        user.getAuthorities().size();   //https://stackoverflow.com/questions/36106620/failed-to-lazily-initialize-a-collection-of-role-user-authorities-could-not-in/36106830
        return user;
    }

}
