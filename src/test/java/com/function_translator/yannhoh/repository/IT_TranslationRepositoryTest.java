package com.function_translator.yannhoh.repository;

import com.function_translator.yannhoh.model.FunctionEntry;
import com.function_translator.yannhoh.model.Language;
import com.function_translator.yannhoh.model.Translation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


@ActiveProfiles("test")
@DataJpaTest
class IT_TranslationRepositoryTest {

    @Autowired
    TranslationRepository translationRepository;

    @Autowired
    FunctionEntryRepository functionEntryRepository;

    @Autowired
    UserRepository userRepository;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void findPublicTranslation() {
        FunctionEntry functionA = functionEntryRepository.getById(100L);
        var tList = translationRepository.findTranslationByFunctionsContainsAndEnabledIsTrue(functionA).get();
        assertEquals(2, tList.size());
        for(Translation t: tList){
            assertTrue(t.isEnabled());
        }
    }

    @Test
    void findTranslationByFunctionsContainsAndFunctionsContains() {
        //2 functions included in a saved translation ->Should find a translation
        FunctionEntry functionA = functionEntryRepository.getById(100L);
        FunctionEntry functionB = functionEntryRepository.getById(101L);
        var translation = translationRepository.findTranslationByFunctionsContainsAndFunctionsContains(
                functionA, functionB);
        assertTrue(translation.isPresent());
        //1 function included in a saved translation ->should not find a translation
        FunctionEntry functionC = functionEntryRepository.getById(103L);
        translation = translationRepository.findTranslationByFunctionsContainsAndFunctionsContains(
                functionA, functionC);
        assertFalse(translation.isPresent());
        //No function included in a saved translation
        FunctionEntry functionD = new FunctionEntry("none", "of", "a", Language.JAVA, "function");
        functionD.setId(666);
        FunctionEntry functionE = new FunctionEntry("which", "does", "infact", Language.PYTHON, "exist");
        functionE.setId(777777);
        translation = translationRepository.findTranslationByFunctionsContainsAndFunctionsContains(
                functionD, functionE);
        assertFalse(translation.isPresent());
    }

    @Test
    void findAllByUsername() {
        var list = translationRepository.findAllByUsername("admin").get();
        assertEquals(list.size(), 2);
        var all = translationRepository.findAll();
        int c = 0;
        for (Translation t : all) {
            if (!t.getUsername().equals("admin")) c++;
        }
        assertEquals(all.size() - c, 2);
    }

    @Test
    void findAllByEnabledIsFalse() {
        var list = translationRepository.findAllByEnabledIsFalse().get();
        assertEquals(3, list.size());
        var all = translationRepository.findAll();
        int c = 0;
        for (Translation t : all) {
            if (t.isEnabled()) c++;
        }
        assertEquals(3, all.size() - c);
    }

    @Test
    void findById() {
        assertTrue(translationRepository.existsById(200L));
        var translation = translationRepository.findById(200L);
        assertEquals("The python function not only takes string parameter, but also numbers", translation.get().getComment());
        translation = translationRepository.findById(9000L);
        assertTrue(translation.isEmpty());
    }

    @Test
    void deleteById() {
        assertTrue(translationRepository.existsById(200L));
        int translationsCounter = translationRepository.findAll().size();
        translationRepository.deleteById(200L);
        assertEquals(translationsCounter - 1, translationRepository.findAll().size());
        assertFalse(translationRepository.existsById(200L));
        assertThrows(EmptyResultDataAccessException.class, () -> translationRepository.deleteById(200L));
        assertEquals(translationsCounter - 1, translationRepository.findAll().size());

    }

    @Test
    void save() {
        FunctionEntry fA = new FunctionEntry("search_name_A", "full_name_A", "url_a", Language.getLanguage("JAVA"), "UTIL");
        FunctionEntry fB = new FunctionEntry("search_name_B", "full_name_B", "url_b", Language.getLanguage("PYTHON"), "STANDARD");
        Set<FunctionEntry> set = new HashSet<>();
        set.add(functionEntryRepository.save(fA));
        set.add(functionEntryRepository.save(fB));
        Translation tA = new Translation("Melanie", set, "this is a comment.");
        var translation = translationRepository.save(tA);
        assertNotNull(translation);
        assertFalse(translation.isEnabled());

        //overwriting a translation should also overwrite the included functions
        FunctionEntry fAA = functionEntryRepository.getById(fA.getId());
        FunctionEntry fBB = functionEntryRepository.getById(fB.getId());
        fAA.setDocUrl("someURL.com");
        fBB.setDocUrl("someOtherURL.com");
        Translation tAA = translationRepository.getById(tA.getId());
        tAA.setComment("a new one");
        tAA.setEnabled(true);
        set.clear();
        set.add(fAA);
        set.add(fBB);
        tAA.setFunctions(set);
        translationRepository.save(tAA);
        Translation updated = translationRepository.getById(tA.getId());
        assertEquals(updated.getComment(), "a new one");
        assertTrue(tAA.isEnabled());
        int c = 0;
        for(FunctionEntry f : translation.getFunctions()) {
            if(f.getDocUrl().equals("someURL.com")) {
                c++;
                assertEquals(fA.getId(), f.getId());
            }
            if(f.getDocUrl().equals("someOtherURL.com")) {
                c++;
                assertEquals(fB.getId(), f.getId());
            }
        }
        assertEquals(2, c);
    }
}