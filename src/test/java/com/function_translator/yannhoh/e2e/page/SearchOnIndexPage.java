package com.function_translator.yannhoh.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class SearchOnIndexPage extends IndexPage {
    private Select selectLanguageIn;
    private Select selectLanguageOut;
    @FindBy(css = ".header__menu-item-li")
    private List<WebElement> indexHeaderElement;
    @FindBy(css = "#search-result-area")
    private WebElement searchResultElement;
    @FindBy(id = "language_in")
    private WebElement selectLanguageInElement;
    @FindBy(id = "language_out")
    private WebElement selectLanguageOutElement;
    @FindBy(css = ".button-submit")
    private WebElement submitButtonElement;
    @FindBy(name = "search_name")
    private WebElement searchTextElement;

    public SearchOnIndexPage(WebDriver webDriver, int port) {
        super(webDriver, port);
        this.selectLanguageIn = new Select(selectLanguageInElement);
        this.selectLanguageOut = new Select(selectLanguageOutElement);
    }

    public AbstractPage searchTranslation(String languageIn, String searchText, String languageOut) {
        this.setLanguageIn(languageIn);
        this.setSearchText(searchText);
        this.setLanguageOut(languageOut);

        return this.submitForm();
    }

    public void setLanguageIn(String languageIn) {
        this.selectLanguageIn.selectByVisibleText(languageIn);
    }

    public void setLanguageOut(String languageOut) {
        this.selectLanguageOut.selectByVisibleText(languageOut);
    }

    public void setSearchText(String searchText) {
        this.searchTextElement.clear();
        this.searchTextElement.sendKeys(searchText);
    }

    public AbstractPage submitForm() {
        this.submitButtonElement.click();
        if (this.webDriver.getCurrentUrl().contains("/translation/search")) {
            return this;
        } else {
            return new SearchOnIndexPage(this.webDriver, this.port);
        }
    }
}


