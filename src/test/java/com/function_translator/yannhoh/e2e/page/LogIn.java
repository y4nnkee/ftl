package com.function_translator.yannhoh.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LogIn extends LoginPage {
    @FindBy(id = "user-username")
    private WebElement usernameInputElement;
    @FindBy(id = "user-password")
    private WebElement passwordInputElement;
    @FindBy(className = "button-login")
    private WebElement submitButtonElement;

    public LogIn(WebDriver webDriver, int port) {
        super(webDriver, port);
    }

    public AbstractPage login(String username, String password) {
        this.setUserName(username);
        this.setPassword(password);

        return this.submitForm();
    }

    public void setUserName(String username) {
        this.usernameInputElement.clear();
        this.usernameInputElement.sendKeys(username);
    }

    public void setPassword(String password) {
        this.passwordInputElement.clear();
        this.passwordInputElement.sendKeys(password);
    }

    public AbstractPage submitForm() {
        this.submitButtonElement.click();
        if (this.webDriver.getCurrentUrl().contains("/login")) {
            return this;
        } else {
            return new LogIn(this.webDriver, this.port);
        }
    }
}


