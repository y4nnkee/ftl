package com.function_translator.yannhoh.e2e.page;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class UserContributePage extends AbstractPage {

    @FindBy(id = "update_id")
    private List<WebElement> updateFormElement;

    public UserContributePage(WebDriver webDriver, int port) {
        super(webDriver, port);
    }

    public boolean updateFormContainsTranslation(long id) {
        WebElement formElement;
        try {
            formElement = webDriver.findElement(By.name("translation_div_" + id));
        } catch (NoSuchElementException e) {
            return false;
        }
        return this.updateFormElement.contains(formElement);
    }
}
