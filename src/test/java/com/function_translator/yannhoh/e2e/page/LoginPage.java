package com.function_translator.yannhoh.e2e.page;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class LoginPage extends AbstractPage {

    @FindBy(css = ".header__menu-item-li")
    private List<WebElement> indexHeaderElement;

    public LoginPage(WebDriver webDriver, int port) {
        super(webDriver, port);
    }

    public boolean isLoggedIn() {
        List<String> headerList = null;
        try {
            headerList = this.indexHeaderElement.stream().map(WebElement::getText).toList();
        } catch (NoSuchElementException e) {
            return false;
        }
        return (headerList.contains("Profile") && headerList.contains("Contribute"));
    }
}
