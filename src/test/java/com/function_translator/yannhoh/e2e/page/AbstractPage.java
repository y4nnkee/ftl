package com.function_translator.yannhoh.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;

abstract public class AbstractPage {
    protected final WebDriver webDriver;
    protected final int port;

    public AbstractPage(WebDriver webDriver, int port) {
        this.webDriver = webDriver;
        this.port = port;

        PageFactory.initElements(webDriver, this);
    }

    public void goToIndexPage() {
        this.webDriver.navigate().to(this.getUriBuilder().path("/").build().toString());
        new IndexPage(this.webDriver, this.port);
    }

    public SearchOnIndexPage goToIndexForSearch() {
        this.webDriver.navigate().to(this.getUriBuilder().path("/").build().toString());
        return new SearchOnIndexPage(this.webDriver, this.port);
    }

    public ContributePage goToContributePage() {
        this.webDriver.navigate().to(this.getUriBuilder().path("/user/contribute").build().toString());
        return new ContributePage(this.webDriver, this.port);
    }

    public LogIn goToLogin() {
        this.webDriver.navigate().to(this.getUriBuilder().path("/login").build().toString());
        return new LogIn(this.webDriver, this.port);
    }

    public UriBuilder getUriBuilder() {
        return UriComponentsBuilder.fromUriString("http://localhost:%d/".formatted(this.port));
    }
}
