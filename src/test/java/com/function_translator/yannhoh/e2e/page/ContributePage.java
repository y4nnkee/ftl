package com.function_translator.yannhoh.e2e.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ContributePage extends UserContributePage {

    public ContributePage(WebDriver webDriver, int port) {
        super(webDriver, port);
    }

    public AbstractPage deleteTranslation(long id) {
        this.clickEdit(id);
        return this.submitFormDelete(id);
    }

    public void clickEdit(long id) {
        WebElement editButtonElement = webDriver.findElement(By.id("edit-button_" + id));
        editButtonElement.click();
    }

    public AbstractPage submitFormDelete(long id) {
        WebElement deleteButtonElement = webDriver.findElement(By.id("delete-button_" + id));
        deleteButtonElement.click();
//        webDriver.switchTo().alert().accept(); //Apparently no alert with this web driver
        if (this.webDriver.getCurrentUrl().contains("/user/contribute")) {
            return this;
        } else {
            return new ContributePage(this.webDriver, this.port);
        }
    }
}


