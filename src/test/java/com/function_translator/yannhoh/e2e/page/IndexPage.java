package com.function_translator.yannhoh.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class IndexPage extends AbstractPage {

    @FindBy(css = ".header__menu-item-li")
    private List<WebElement> indexHeaderElement;
    @FindBy(css = "#search-result-area")
    private WebElement searchResultElement;
    @FindBy(id = "language_in")
    private WebElement selectLanguageInElement;
    @FindBy(id = "language_out")
    private WebElement selectLanguageOutElement;
    @FindBy(css = ".button-submit")
    private WebElement submitButtonElement;
    @FindBy(css = ".search-name")
    private WebElement searchTextElement;


    public IndexPage(WebDriver webDriver, int port) {
        super(webDriver, port);
    }

    public List<String> getHeaderElements() {
        return this.indexHeaderElement.stream().map(WebElement::getText).toList();
    }

    public String getSearchResult() {
        return this.searchResultElement.getText();
    }

    public String getSearchText() {
        return this.searchTextElement.getAttribute("value");
    }

    public String getSelectedLanguageIn() {
        Select select = new Select(this.selectLanguageInElement);
        return select.getFirstSelectedOption().getText();
    }

    public String getSelectedLanguageOut() {
        Select select = new Select(this.selectLanguageOutElement);
        return select.getFirstSelectedOption().getText();
    }

    public List<String> getAllLanguageInOptions() {
        Select select = new Select(this.selectLanguageInElement);
        return select.getOptions().stream().map(WebElement::getText).toList();
    }

    public List<String> getAllLanguageOutOptions() {
        Select select = new Select(this.selectLanguageOutElement);
        return select.getOptions().stream().map(WebElement::getText).toList();
    }
}
