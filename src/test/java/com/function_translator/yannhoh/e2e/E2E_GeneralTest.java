package com.function_translator.yannhoh.e2e;

import com.function_translator.yannhoh.e2e.page.IndexPage;
import com.function_translator.yannhoh.e2e.page.LoginPage;
import com.function_translator.yannhoh.e2e.page.UserContributePage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@Import(WebDriverConfiguration.class)
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class E2E_GeneralTest {
    @LocalServerPort
    private int port;
    @Autowired
    private WebDriver webDriver;
    private IndexPage indexPage;

    @BeforeEach
    public void setUp() {
        this.indexPage = new IndexPage(this.webDriver, this.port);
    }

    @Test
    public void testShowIndex() {
        this.indexPage.goToIndexPage();
        assertEquals(2, this.indexPage.getHeaderElements().size());
        assertTrue(this.indexPage.getHeaderElements().contains("About"));
        assertTrue(this.indexPage.getHeaderElements().contains("Login"));
        assertTrue(this.indexPage.getSearchResult().isBlank());
        assertTrue(this.indexPage.getSearchText().isBlank());
        assertTrue(this.indexPage.getAllLanguageInOptions().contains("JAVA"));
        assertTrue(this.indexPage.getAllLanguageInOptions().contains("PYTHON"));
        assertTrue(this.indexPage.getAllLanguageOutOptions().contains("JAVA"));
        assertTrue(this.indexPage.getAllLanguageOutOptions().contains("PYTHON"));
    }

    @Test
    public void testSearchTranslation() {
        var searchOnIndexPage = this.indexPage.goToIndexForSearch();
        var abstractPage = searchOnIndexPage.searchTranslation("JAVA", "getClass", "PYTHON");
        if (abstractPage instanceof IndexPage indexPage) {
            assertTrue(indexPage.getSearchResult().contains("PYTHON"));
            assertTrue(indexPage.getSearchResult().contains("STANDARD"));
            assertTrue(indexPage.getSearchResult().contains("type(object)"));
            assertTrue(indexPage.getSearchResult().contains("https://docs.python.org/3/library/functions.html#type"));
        } else {
            fail();
        }
        abstractPage = searchOnIndexPage.searchTranslation("JAVA", "something", "PYTHON");
        if (abstractPage instanceof IndexPage indexPage) {
            assertTrue(indexPage.getSearchResult().contains("Function something not found"));
        } else {
            fail();
        }
    }

    @Test
    public void login() {
        var loginPage = this.indexPage.goToLogin();
        var abstractPage = loginPage.login("admin", "123");
        if (abstractPage instanceof LoginPage logIn) {
            assertTrue(logIn.isLoggedIn());
            assertTrue(webDriver.getCurrentUrl().contains("/user/profile"));
        } else {
            fail();
        }
    }

    @Test
    void loginWrongPassword() {
        var loginPage = this.indexPage.goToLogin();
        var abstractPage = loginPage.login("admin", "12345678910111213");
        if (abstractPage instanceof LoginPage logIn) {
            assertFalse(logIn.isLoggedIn());
            assertFalse(webDriver.getCurrentUrl().contains("/user/profile"));
            assertTrue(webDriver.getCurrentUrl().contains("/login"));
        } else {
            fail();
        }
    }

    @Test
    public void deleteTranslation() {
        login();
        var contributePage = this.indexPage.goToContributePage();
        System.out.println("URL " + webDriver.getCurrentUrl());
        var abstractPage = contributePage.deleteTranslation(202L);
        if (abstractPage instanceof UserContributePage userContributePage) {
            assertFalse(userContributePage.updateFormContainsTranslation(202L));
        } else {
            fail();
        }
    }

    @AfterEach
    public void tearDown() {
        this.webDriver.quit();
    }
}
