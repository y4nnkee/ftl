package com.function_translator.yannhoh.controller;

import com.function_translator.yannhoh.exception.APINotFoundException;
import com.function_translator.yannhoh.model.FunctionEntry;
import com.function_translator.yannhoh.model.Language;
import com.function_translator.yannhoh.model.Translation;
import com.function_translator.yannhoh.model.User;
import com.function_translator.yannhoh.repository.FunctionEntryRepository;
import com.function_translator.yannhoh.repository.TranslationRepository;
import com.function_translator.yannhoh.repository.UserRepository;
import com.function_translator.yannhoh.service.TranslationService;
import com.function_translator.yannhoh.service.UserService;
import com.mitchellbosecke.pebble.boot.autoconfigure.PebbleAutoConfiguration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TranslationController.class)
@Import(PebbleAutoConfiguration.class)
class IT_TranslationControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TranslationService translationService;
    @MockBean
    private TranslationRepository translationRepository;
    @MockBean
    private FunctionEntryRepository functionEntryRepository;
    @MockBean
    private UserService userService;
    @MockBean
    private UserRepository userRepository;

    private String fromLanguage, toLanguage, searchName;
    private Translation tA;
    private Set<FunctionEntry> set;
    private FunctionEntry functionFrom;
    private FunctionEntry functionTo;
    private User userA;

    @BeforeEach
    void setUp() {
        searchName = "TestSearchName";
        fromLanguage = "JAVA";
        toLanguage = "PYTHON";
        userA = new User("DonKnow", "Don");
        functionFrom = new FunctionEntry("search_name_A", "full_name_A", "url_a", Language.getLanguage(fromLanguage), "UTIL");
        functionTo = new FunctionEntry("search_name_B", searchName, "url_b", Language.getLanguage(toLanguage), "STANDARD");
        set = new HashSet<>();
        set.add(functionFrom);
        set.add(functionTo);
        tA = new Translation(userA.getUsername(), set, "this is a comment.");
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void searchTranslation() throws Exception {
        //Translation is found
        when(translationService.searchFunction(searchName, fromLanguage)).thenReturn(functionFrom);
        when(translationService.searchTranslation(functionFrom, toLanguage)).thenReturn(tA);
        this.mockMvc.perform(get(
                        "/translation/search?search_name=" + searchName + "&language_in=" + fromLanguage + "&language_out=" + toLanguage))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(functionTo.getFullName())))
                .andExpect(content().string(containsString(functionTo.getLanguage())))
                .andExpect(content().string(containsString(functionTo.getDocUrl())))
                .andExpect(content().string(containsString(functionTo.getLibrary())));
        //Translation is not found
        Mockito.reset(translationRepository, translationService);
        when(translationService.searchFunction("something", fromLanguage)).thenThrow(new APINotFoundException("Function something not found"));
        this.mockMvc.perform(get(
                        "/translation/search?search_name=" + "something" + "&language_in=" + fromLanguage + "&language_out=" + toLanguage))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Function something not found")));
        //Parameter not complete - BadRequest
        Mockito.reset(translationRepository, translationService);
        when(translationService.searchFunction("something", fromLanguage)).thenThrow(new APINotFoundException("Function something not found"));
        this.mockMvc.perform(get(
                        "/translation/search?search_name=hola"))
                .andExpect(status().isBadRequest());
    }
}