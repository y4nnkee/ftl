package com.function_translator.yannhoh.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.function_translator.yannhoh.exception.APIAlreadyReportedException;
import com.function_translator.yannhoh.exception.APINotFoundException;
import com.function_translator.yannhoh.model.FunctionEntry;
import com.function_translator.yannhoh.model.Language;
import com.function_translator.yannhoh.model.Translation;
import com.function_translator.yannhoh.model.User;
import com.function_translator.yannhoh.repository.FunctionEntryRepository;
import com.function_translator.yannhoh.repository.TranslationRepository;
import com.function_translator.yannhoh.repository.UserRepository;
import com.function_translator.yannhoh.service.TranslationService;
import com.function_translator.yannhoh.service.UserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Http-request to Controller
 */


@ActiveProfiles("test")
@WebMvcTest(TranslationRestController.class)
class IT_TranslationRestControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    TranslationService translationService;
    @MockBean
    UserRepository userRepository;
    @MockBean
    UserService userService;
    @MockBean
    private TranslationRepository translationRepository;
    @MockBean
    private FunctionEntryRepository functionEntryRepository;

    private JacksonTester<Translation> translationJacksonTester;
    private Translation tA;
    private Set<FunctionEntry> set;
    private FunctionEntry fA;
    private FunctionEntry fB;
    private User userA;
    private String url;

    @BeforeEach
    void setUp() {
        Mockito.reset(translationService, userRepository, userService);
        JacksonTester.initFields(this, new ObjectMapper());
        userA = new User("DonKnow", "Don");
        fA = new FunctionEntry("search_name_A", "full_name_A", "url_a", Language.getLanguage("JAVA"), "UTIL");
        fA.setId(11L);
        fB = new FunctionEntry("search_name_B", "full_name_B", "url_b", Language.getLanguage("PYTHON"), "STANDARD");
        fB.setId(22L);
        set = new HashSet<>();
        set.add(fA);
        set.add(fB);
        tA = new Translation(userA.getUsername(), set, "this is a comment.");
        tA.setId(99L);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void getAllLanguages() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/api/translation/languages")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON);
        MvcResult response = this.mockMvc.perform(builder)
                .andReturn();
        assertTrue(response.getResponse().getContentAsString().contains("JAVA"));
        assertTrue(response.getResponse().getContentAsString().contains("UTIL"));
        assertTrue(response.getResponse().getContentAsString().contains("PYTHON"));
        assertTrue(response.getResponse().getContentAsString().contains("STANDARD"));
    }

    @Test
    public void deleteTranslation() throws Exception {
        //Legit id
        when(this.translationService.deleteTranslationId(tA.getId())).thenReturn(tA.getId());
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.delete("/api/translation")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf())
                .with(user("admin").roles("ADMIN"))
                .content(String.valueOf(tA.getId()));
        this.mockMvc.perform(builder)
                .andExpect(status().isAccepted())
                .andExpect(r -> assertEquals(r.getResponse().getContentAsString(), String.valueOf(tA.getId())));
        //Wrong id
        Mockito.reset(translationService);
        when(this.translationService.deleteTranslationId(tA.getId())).thenThrow(new APINotFoundException("No translations with this id found"));
        builder = MockMvcRequestBuilders.delete("/api/translation")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf())
                .with(user("admin").roles("ADMIN"))
                .content(String.valueOf(tA.getId()));
        this.mockMvc.perform(builder)
                .andExpect(status().isNotFound())
                .andExpect(r -> assertEquals(r.getResponse().getContentAsString(), String.valueOf(tA.getId())));
        //No csrf token
        builder = MockMvcRequestBuilders.delete("/api/translation")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .with(user("admin").roles("ADMIN"))
                .content(String.valueOf(tA.getId()));
        this.mockMvc.perform(builder)
                .andExpect(status().isForbidden());
    }

    @Test
    public void publishTranslationTest() throws Exception {
        //publish translation as it is already saved
        when(this.translationService.publishTranslation(tA)).thenReturn(tA);
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/api/translation")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf())
                .with(user("admin").roles("ADMIN"))
                .content(this.translationJacksonTester.write(tA).getJson());
        this.mockMvc.perform(builder)
                .andExpect(status().isAccepted());

        //Translation without Functions
        tA.setFunctions(new HashSet<>());
        MockHttpServletRequestBuilder builder2 = MockMvcRequestBuilders.put("/api/translation")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf())
                .with(user("admin").roles("ADMIN"))
                .content(this.translationJacksonTester.write(tA).getJson());
        this.mockMvc.perform(builder2)
                .andExpect(status().isBadRequest());

        //Not enough permission
        Mockito.reset(translationService);
        when(this.translationService.publishTranslation(tA)).thenReturn(tA);
        MockHttpServletRequestBuilder builder3 = MockMvcRequestBuilders.put("/api/translation")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf())
                .with(user("superHer0").roles("CONTRIBUTOR"))
                .content(this.translationJacksonTester.write(tA).getJson());
        this.mockMvc.perform(builder3)
                .andExpect(status().isBadRequest());

    }

    @WithMockUser(value = "spring")
    @Test
    public void addNewTranslation() throws Exception {
        MockHttpServletRequestBuilder builder;
        //Translation without id's
        fA = new FunctionEntry("search_name_A", "full_name_A", "url_a", Language.getLanguage("JAVA"), "UTIL");
        fB = new FunctionEntry("search_name_B", "full_name_B", "url_b", Language.getLanguage("PYTHON"), "STANDARD");
        set = new HashSet<>();
        set.add(fA);
        set.add(fB);
        tA = new Translation(userA.getUsername(), set, "this is a comment.");
        //Accepted new translation
        Mockito.reset(translationService);
        when(translationService.addNewTranslation(any(), any())).thenReturn(tA);
        builder =
                MockMvcRequestBuilders.post("/api/translation")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf())
                        .with(user("superHer0").roles("CONTRIBUTOR"))
                        .content(this.translationJacksonTester.write(tA).getJson());
        this.mockMvc.perform(builder)
                .andExpect(status().isCreated())
                .andExpect(result -> assertEquals("Thank you for contributing a new translation. You can now see it under \"Profile\". It has to be checked by a admin to get public.", result.getResponse().getContentAsString()));
        //Not accepted translation(409 conflict - already reported)
        Mockito.reset(translationService);
        when(translationService.addNewTranslation(any(), any())).thenThrow(new APIAlreadyReportedException("Translation already reported"));
        builder =
                MockMvcRequestBuilders.post("/api/translation")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf())
                        .with(user("superHer0").roles("CONTRIBUTOR"))
                        .content(this.translationJacksonTester.write(tA).getJson());
        this.mockMvc.perform(builder)
                .andExpect(status().isConflict())
                .andExpect(result -> assertEquals("Translation already reported", result.getResponse().getContentAsString()));

        //Not complete translation (400 bad request)
        Mockito.reset(translationService);
        when(translationService.addNewTranslation(any(), any())).thenThrow(new APIAlreadyReportedException("Translation already reported"));
        tA.setFunctions(new HashSet<>());
        builder =
                MockMvcRequestBuilders.post("/api/translation")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf())
                        .with(user("superHer0").roles("CONTRIBUTOR"))
                        .content(this.translationJacksonTester.write(tA).getJson());
        this.mockMvc.perform(builder)
                .andExpect(status().isBadRequest());
    }

}