package com.function_translator.yannhoh.controller;

import com.function_translator.yannhoh.model.FunctionEntry;
import com.function_translator.yannhoh.model.Language;
import com.function_translator.yannhoh.model.Translation;
import com.function_translator.yannhoh.model.User;
import com.function_translator.yannhoh.repository.FunctionEntryRepository;
import com.function_translator.yannhoh.repository.TranslationRepository;
import com.function_translator.yannhoh.repository.UserRepository;
import com.function_translator.yannhoh.service.TranslationService;
import com.function_translator.yannhoh.service.UserService;
import com.mitchellbosecke.pebble.boot.autoconfigure.PebbleAutoConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
@Import(PebbleAutoConfiguration.class)
class IT_UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TranslationService translationService;
    @MockBean
    private TranslationRepository translationRepository;
    @MockBean
    private FunctionEntryRepository functionEntryRepository;
    @MockBean
    private UserService userService;
    @MockBean
    private UserRepository userRepository;

    private Translation tA;
    private Set<FunctionEntry> set;
    private FunctionEntry fA;
    private FunctionEntry fB;
    private User userA;

    @BeforeEach
    void setup() {
        userA = new User("DonKnow", "Don");
        fA = new FunctionEntry("search_name_A", "full_name_A", "url_a", Language.getLanguage("JAVA"), "UTIL");
        fB = new FunctionEntry("search_name_B", "full_name_B", "url_b", Language.getLanguage("PYTHON"), "STANDARD");
        set = new HashSet<>();
        set.add(fA);
        set.add(fB);
        tA = new Translation(userA.getUsername(), set, "this is a comment.");
    }

    @WithMockUser(value = "DonKnow")
    @Test
    void userHome() throws Exception {
        when(translationService.getAllByUsername(userA.getUsername())).thenReturn(List.of(tA));
        var i = this.mockMvc.perform(get("/user/profile"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("All your contributed translations:")))
                .andExpect(content().string(containsString("DonKnow")))
                .andReturn();
        assertFalse(i.getResponse().getContentAsString().contains("About-Page"));
    }

    @WithMockUser(value = "DonKnow")
    @Test
    void userContribute() throws Exception {
        when(translationService.getAllUnpublic()).thenReturn(List.of(tA));
        var i = this.mockMvc.perform(get("/user/contribute"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Contribute a new Translation")))
                .andReturn();
        assertFalse(i.getResponse().getContentAsString().contains("About-Page"));
    }
}