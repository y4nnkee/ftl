package com.function_translator.yannhoh.controller;

import com.function_translator.yannhoh.repository.FunctionEntryRepository;
import com.function_translator.yannhoh.repository.TranslationRepository;
import com.function_translator.yannhoh.repository.UserRepository;
import com.function_translator.yannhoh.service.TranslationService;
import com.function_translator.yannhoh.service.UserService;
import com.mitchellbosecke.pebble.boot.autoconfigure.PebbleAutoConfiguration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TemplateController.class)
@Import(PebbleAutoConfiguration.class)
class IT_TemplateControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TranslationService translationService;
    @MockBean
    private TranslationRepository translationRepository;
    @MockBean
    private FunctionEntryRepository functionEntryRepository;
    @MockBean
    private UserService userService;
    @MockBean
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void index() throws Exception {
        var i = this.mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Function Translator")))
                .andExpect(content().string(containsString("language_in")))
                .andExpect(content().string(containsString("search_name")))
                .andExpect(content().string(containsString("language_out")))
                .andReturn();
        assertFalse(i.getResponse().getContentAsString().contains("<h1>About</h1>"));
    }

    @Test
    void about() throws Exception {
        var i = this.mockMvc.perform(get("/about"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<h3>Was?</h3>\n" +
                        "    Diese Webapplikation kann einzelne Funktionen einer Programmiersprache in eine andere Programmiersprache übersetzen.\n" +
                        "    Zusätzlich können registrierte Benutzer neue Übersetzungen hinzufügen.")))
                .andReturn();
        assertFalse(i.getResponse().getContentAsString().contains("language_in"));
    }
}