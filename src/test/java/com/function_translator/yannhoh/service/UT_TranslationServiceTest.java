package com.function_translator.yannhoh.service;

import com.function_translator.yannhoh.exception.APIAlreadyReportedException;
import com.function_translator.yannhoh.exception.APIException;
import com.function_translator.yannhoh.exception.APINotFoundException;
import com.function_translator.yannhoh.model.FunctionEntry;
import com.function_translator.yannhoh.model.Language;
import com.function_translator.yannhoh.model.Translation;
import com.function_translator.yannhoh.model.User;
import com.function_translator.yannhoh.repository.FunctionEntryRepository;
import com.function_translator.yannhoh.repository.TranslationRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.test.context.ActiveProfiles;

import java.sql.Date;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
class UT_TranslationServiceTest {

    @InjectMocks
    private TranslationRepository translationRepository;
    @InjectMocks
    private FunctionEntryRepository functionEntryRepository;

    private TranslationService translationService;

    private Translation tA;
    private Set<FunctionEntry> set;
    private FunctionEntry fA;
    private FunctionEntry fB;
    private User userA;

    @BeforeEach
    void setUp() {
        functionEntryRepository = mock(FunctionEntryRepository.class);
        translationRepository = mock(TranslationRepository.class);
        translationService = new TranslationService(translationRepository, functionEntryRepository);
        userA = new User("DonKnow", "Don");
        fA = new FunctionEntry("search_name_a", "full_name_A", "url_a", Language.getLanguage("JAVA"), "UTIL");
        fB = new FunctionEntry("search_name_b", "full_name_B", "url_b", Language.getLanguage("PYTHON"), "STANDARD");
        set = new HashSet<>();
        set.add(fA);
        set.add(fB);
        tA = new Translation(userA.getUsername(), set, "this is a comment.");
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void searchFunction() {
        when(functionEntryRepository.findBySearchNameAndLanguage(fA.getSearchName(), fA.getLanguage())).thenReturn(Optional.of(fA));
        var funcResult = translationService.searchFunction(".search_name_A()", "JAVA");
        assertEquals(funcResult.getSearchName(), fA.getSearchName());
        assertEquals(funcResult.getFullName(), fA.getFullName());
        assertEquals(funcResult.getDocUrl(), fA.getDocUrl());
        assertEquals(funcResult.getLanguage(), fA.getLanguage());
        assertEquals(funcResult.getLibrary(), fA.getLibrary());
    }

//    @Test
//    public void searchTranslation() {
//        when(translationRepository.findTranslationByFunctionsContainsAndFunctionsContains(any(), any()))
//                .thenReturn(Optional.of(tA));
////        when(translationRepository.findById(tA.getId())).thenReturn(Optional.of(tA));
//    }

    @Test
    public void deleteTranslation() throws Exception {
        //Id exists
        when(translationRepository.findById(tA.getId())).thenReturn(Optional.of(tA));
        translationRepository.deleteById(tA.getId());
        verify(translationRepository, times(1)).deleteById(tA.getId());

        //Id doesn't exist
        Mockito.reset(translationRepository, functionEntryRepository);
        assertThrows(APINotFoundException.class, () -> {
            translationService.deleteTranslationId(tA.getId());
        });
        verify(translationRepository, times(0)).deleteById(any());

    }

    @Test
    public void publishTranslation() {
        //publish translation as it is already saved
        fA.setId(11L);
        fB.setId(22L);
        tA.setId(99L);
        when(translationRepository.findById(tA.getId())).thenReturn(Optional.of(tA));
        when(translationRepository.save(tA)).thenReturn(tA);
        when(functionEntryRepository.findById(fA.getId())).thenReturn(Optional.of(fA));
        when(functionEntryRepository.findById(fB.getId())).thenReturn(Optional.of(fB));
        var result = translationService.publishTranslation(tA);
        assertNotNull(result);
        assertEquals("this is a comment.", result.getComment());
        assertEquals("DonKnow", result.getUsername());
        assertEquals(Date.valueOf(LocalDate.now()).toString(), result.getCreatedOn());
        int a = 0;
        int b = 0;
        for (FunctionEntry f : result.getFunctions()) {
            if (f.getSearchName().equals("search_name_a") && f.getFullName().equals("full_name_A")
                    && f.getDocUrl().equals("url_a") && f.getLanguage().equals("JAVA") && f.getLibrary().equals("UTIL")) {
                a++;
            }
            if (f.getSearchName().equals("search_name_b") && f.getFullName().equals("full_name_B")
                    && f.getDocUrl().equals("url_b") && f.getLanguage().equals("PYTHON") && f.getLibrary().equals("STANDARD")) {
                b++;
            }
        }
        assertEquals(1, a);
        assertEquals(1, b);

        //Wrong id on one function
        Mockito.reset(translationRepository);
        Mockito.reset(functionEntryRepository);
        when(translationRepository.findById(tA.getId())).thenReturn(Optional.of(tA));
        when(translationRepository.save(tA)).thenReturn(tA);
        when(functionEntryRepository.findById(fA.getId())).thenReturn(Optional.empty());
        when(functionEntryRepository.findById(fB.getId())).thenReturn(Optional.of(fB));
        var exception = assertThrows(APINotFoundException.class, () -> {
            translationService.publishTranslation(tA);
        });
        assertEquals("No function with this id found", exception.getMessage());

        //Wrong id on translation
        Mockito.reset(translationRepository, functionEntryRepository);
        when(translationRepository.findById(tA.getId())).thenReturn(Optional.empty());
        when(translationRepository.save(tA)).thenReturn(tA);
        when(functionEntryRepository.findById(fA.getId())).thenReturn(Optional.of(fA));
        when(functionEntryRepository.findById(fB.getId())).thenReturn(Optional.of(fB));
        exception = assertThrows(APINotFoundException.class, () -> {
            translationService.publishTranslation(tA);
        });
        assertEquals("No translation with this id found", exception.getMessage());

        //Translation without Functions
        Mockito.reset(translationRepository, functionEntryRepository);
        when(translationRepository.findById(tA.getId())).thenReturn(Optional.of(tA));
        when(functionEntryRepository.findById(fA.getId())).thenReturn(Optional.of(fA));
        when(functionEntryRepository.findById(fB.getId())).thenReturn(Optional.of(fB));
        tA.setFunctions(new HashSet<>());
        var exception2 = assertThrows(APIException.class, () -> {
            translationService.publishTranslation(tA);
        });
        assertEquals("function set not complete", exception2.getMessage());
        verify(translationRepository, times(0)).save(any());

    }

    @Test
    public void addNewTranslation() {
        //Add a Translation with functions which already exists - Translation should not be saved
        when(functionEntryRepository.findBySearchNameAndLanguage(fA.getSearchName(), fA.getLanguage())).thenReturn(Optional.of(fA));
        when(functionEntryRepository.findBySearchNameAndLanguage(fB.getSearchName(), fB.getLanguage())).thenReturn(Optional.of(fB));
        when(translationRepository.findTranslationByFunctionsContainsAndFunctionsContains(fA, fB)).thenReturn(Optional.of(tA));
        when(translationRepository.findTranslationByFunctionsContainsAndFunctionsContains(fB, fA)).thenReturn(Optional.of(tA));
        Exception exception = assertThrows(APIAlreadyReportedException.class, () -> {
            translationService.addNewTranslation(tA, userA.getUsername());
        });
        assertTrue(exception.getMessage().equals("Translation with the functions " + fA.getFullName() + " and " + fB.getFullName() + " is already reported") ||
                exception.getMessage().equals("Translation with the functions " + fB.getFullName() + " and " + fA.getFullName() + " is already reported"));
        verify(translationRepository, times(0)).save(any());

        //One Function is not saved already - Translation should be saved
        Mockito.reset(functionEntryRepository, translationRepository);
        when(functionEntryRepository.findBySearchNameAndLanguage(fA.getSearchName(), fA.getLanguage())).thenReturn(Optional.empty());
        when(functionEntryRepository.findBySearchNameAndLanguage(fB.getSearchName(), fB.getLanguage())).thenReturn(Optional.of(fB));
        when(translationRepository.findTranslationByFunctionsContainsAndFunctionsContains(fA, fB)).thenReturn(Optional.empty());
        when(translationRepository.findTranslationByFunctionsContainsAndFunctionsContains(fB, fA)).thenReturn(Optional.empty());
        translationService.addNewTranslation(tA, userA.getUsername());
        verify(translationRepository, times(1)).save(any());

        //One Function has same search-name but another language - Translation and function should be saved
        FunctionEntry fC = fB;
        fC.setLanguage("JAVA");
        Mockito.reset(functionEntryRepository, translationRepository);
        when(functionEntryRepository.findBySearchNameAndLanguage(fA.getSearchName(), fA.getLanguage())).thenReturn(Optional.of(fA));
        when(functionEntryRepository.findBySearchNameAndLanguage(fB.getSearchName(), fB.getLanguage())).thenReturn(Optional.of(fB));
        when(functionEntryRepository.findBySearchNameAndLanguage(fC.getSearchName(), fC.getLanguage())).thenReturn(Optional.empty());
        when(translationRepository.findTranslationByFunctionsContainsAndFunctionsContains(fA, fB)).thenReturn(Optional.of(tA));
        when(translationRepository.findTranslationByFunctionsContainsAndFunctionsContains(fB, fA)).thenReturn(Optional.of(tA));
        when(translationRepository.findTranslationByFunctionsContainsAndFunctionsContains(fA, fC)).thenReturn(Optional.empty());
        when(translationRepository.findTranslationByFunctionsContainsAndFunctionsContains(fC, fA)).thenReturn(Optional.empty());
        Set<FunctionEntry> newSet = new HashSet<>(2);
        newSet.add(fA);
        newSet.add(fC);
        Translation tB = tA;
        tB.setFunctions(newSet);
        translationService.addNewTranslation(tB, userA.getUsername());
        verify(translationRepository, times(1)).save(any());
        verify(functionEntryRepository, times(1)).save(fC);
        verify(functionEntryRepository, times(1)).save(any());
    }

    @Test
    public void getAllLanguages() {
        var f = Language.getAllLanguages();
        assertNotNull(f);
        int c = 0;
        for (String l : f) {
            if (l.equals("JAVA")) {
                c++;
            }
            if (l.equals("PYTHON")) {
                c++;
            }
        }
        assertEquals(2, c);
    }

    @Test
    public void getAllByUsername() throws Exception {
        //User has contributed translations
        List<Translation> list = List.of(tA);
        when(translationRepository.findAllByUsername(userA.getUsername())).thenReturn(Optional.of(list));
        //User has no contributed translations
        Mockito.reset(translationRepository);
        when(translationRepository.findAllByUsername(userA.getUsername())).thenReturn(Optional.empty());
        Exception e = assertThrows(APINotFoundException.class, () -> translationService.getAllByUsername(userA.getUsername()));
        assertEquals("No translations with this user id found", e.getMessage());
    }

    @Test
    public void getAllUnpublic() throws Exception {
        //Unpublic translations exist
        List<Translation> list = List.of(tA);
        when(translationRepository.findAllByEnabledIsFalse()).thenReturn(Optional.of(list));
        var l = translationService.getAllUnpublic();
        //Unpublic translations do not exist
        Mockito.reset(translationRepository);
        when(translationRepository.findAllByEnabledIsFalse()).thenReturn(Optional.empty());
        Exception e = assertThrows(APINotFoundException.class, () -> translationService.getAllUnpublic());
        assertEquals("No unpublic translations found", e.getMessage());
    }
}